import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';


import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { InspectionPage } from '../pages/inspection/inspection';
import { FieldPage } from '../pages/field/field';
import { MapPage } from '../pages/map/map';
import { ModalPage } from '../pages/modal/modal';
import { HistoryPage } from '../pages/history/history';
import { HistoryItemPage } from '../pages/history-item/history-item';
import { EditPage } from '../pages/edit/edit';

import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { GetUserServiceProvider } from '../providers/get-user-service/get-user-service';
import { GuardServiceProvider } from '../providers/guard-service/guard-service';
import { TokenServiceProvider } from '../providers/token-service/token-service';
import { TabsPage } from '../pages/tabs/tabs';
import { GetFieldsProvider } from '../providers/get-fields/get-fields';
import { PhotoServiceProvider } from '../providers/photo-service/photo-service';
import { UrlServiceProvider } from '../providers/url-service/url-service';






@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    InspectionPage,
    FieldPage,
    MapPage,
    TabsPage,
    ModalPage,
    HistoryPage,
    HistoryItemPage,
    EditPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollAssist: true,
      scrollPadding: false,
      autoFocusAssist: false
    }),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    InspectionPage,
    FieldPage,
    MapPage,
    TabsPage,
    ModalPage,
    HistoryPage,
    HistoryItemPage,
    EditPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    GetUserServiceProvider,
    Geolocation,
    GuardServiceProvider,
    TokenServiceProvider,
    GetFieldsProvider,
    PhotoServiceProvider,,
    UrlServiceProvider,
  ]
})
export class AppModule {}
