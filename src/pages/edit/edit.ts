import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { PhotoServiceProvider } from '../../providers/photo-service/photo-service';

@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
  providers: [Camera]
})
export class EditPage {
  report: any;
  photo: any;

  options: CameraOptions = {
    quality: 100,
    targetHeight: 300,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    saveToPhotoAlbum: false,
    allowEdit: false,
    sourceType: 1

  }


  constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera, public events: Events, public zone:NgZone) {
    this.report = navParams.data;
    console.log(this.report)
  }

  private openGallery (): void {

  let cameraOptions = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    quality: 100,
    targetHeight: 300,
    encodingType: this.camera.EncodingType.JPEG,
    correctOrientation: true
  }

  this.camera.getPicture(cameraOptions)
    .then( (file_url) => {
      this.photo = 'data:image/jpeg;base64,' + file_url;
      this.report.src.push(this.photo);
      this.events.publish('updateScreen');
    },
    err => console.log(err));
  }

  back() {
    this.navCtrl.pop();
  }

  takePhoto() {
    this.camera.getPicture(this.options)
      .then((imageData) => {
        this.photo = 'data:image/jpeg;base64,' + imageData;
        this.report.src.push(this.photo);
        this.events.publish('updateScreen');
      },(err) => {
        console.log(err);
     });
  }

  delete(item) {
  this.report.src.forEach( (el , i) => {
    if(el == item) {
      this.report.src.splice(i, 1);
    }
  })
  this.events.publish('updateScreen');
  }

  reported() {
    this.navCtrl.pop();
  }
}
