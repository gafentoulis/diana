import { Component } from '@angular/core';
import { NavController, NavParams, App, ModalController, AlertController  } from 'ionic-angular';

import { ModalPage } from '../modal/modal';
import { HistoryPage } from '../history/history';

import { GuardServiceProvider } from '../../providers/guard-service/guard-service';
import { PhotoServiceProvider } from '../../providers/photo-service/photo-service';

import { LoginPage } from '../login/login';
import { InspectionPage } from '../inspection/inspection';
import leaflet from 'leaflet';



@Component({
  selector: 'page-field',
  templateUrl: 'field.html',
  providers: [GuardServiceProvider, PhotoServiceProvider]
})

export class FieldPage {
  confirm = this.navParams.data.confirm;
  loginPage = LoginPage;
  field = this.navParams.data.field;
  reported: any;
  map: any;


  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private modalCtrl: ModalController,
     private alertCtrl: AlertController,
     private guardServiceProvider: GuardServiceProvider,
     private app: App,
     public photoService: PhotoServiceProvider) {}

  //ION CYCLE
  ionViewWillEnter() {
    this.loadMap();
    this.photoService.getPhoto(this.navParams.data.field.water_rights_italy.id)
    .subscribe(
      res => {
        this.reported = res;
      }
    )
  }

  ionViewWillLeave() {
    if(!this.guardServiceProvider.isAuth()) {
      this.app.getRootNav().push(this.loginPage);
    }
  }

  //PAGE FUNCTIONS
  history() {
    const modal = this.modalCtrl.create(HistoryPage, {
      report: this.reported
    });
    modal.present();
  }
  report() {
    const modal = this.modalCtrl.create(ModalPage, {
      field: this.field
    });
    modal.present();
  }

  loadMap() {
    this.map = leaflet.map("map1").fitWorld();
    this.map.setView([this.field.water_rights_italy.geom[0][0], this.field.water_rights_italy.geom[0][1]], 17);
    leaflet.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ2FmZW50b3VsaXMiLCJhIjoiY2ppcTF1cHE4MHF6YzNxcGVxNmN2NWkwNyJ9.YVhHL3fSXu7S3kKhEmbCpQ', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18,
      minZoom: 3
    }).addTo(this.map);
    this.map.locate({
      maxZoom: 15
    }).on('locationfound', (e) => {
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([e.latitude, e.longitude]);
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup);
    })
    leaflet.polygon(this.field.water_rights_italy.geom).addTo(this.map);
  }

}
