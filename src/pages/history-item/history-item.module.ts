import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryItemPage } from './history-item';

@NgModule({
  declarations: [
    HistoryItemPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryItemPage),
  ],
})
export class HistoryItemPageModule {}
