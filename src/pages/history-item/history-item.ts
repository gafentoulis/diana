import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { EditPage } from '../edit/edit';

@Component({
  selector: 'page-history-item',
  templateUrl: 'history-item.html',
})
export class HistoryItemPage {
  report = this.navParams.data;
  imgs = this.navParams.data.src;
  imgss :any
  title: string = 'Lorem ipsum dolor sit amet.';
  checked = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.imgss = this.navParams.data.report;
    console.log(this.imgss);
  }

  getPhotos() {
    this.checked = !this.checked;
  }

  edit() {
   let profileModal = this.modalCtrl.create(EditPage, {
     text: this.navParams.data.text,
     src: this.imgs,
     title: this.title
   });
   profileModal.present();
 }

}
