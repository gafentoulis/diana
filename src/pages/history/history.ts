import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HistoryItemPage } from '../history-item/history-item';
import * as $ from 'jquery'

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  animateCheck: boolean = false;
  reports = [{src:["assets/imgs/farm1.jpeg", "assets/imgs/farm2.jpeg"], date: '17/04/16', user: window.localStorage.getItem('user'), text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsum eum molestiae at autem provident aliquam, impedit libero. Quia, itaque.'},
  {src:["assets/imgs/farm2.jpeg"], date: '17/08/16', user: window.localStorage.getItem('user'), text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsum eum molestiae at autem provident aliquam, impedit libero. Quia, itaque.'},
  {src:["assets/imgs/farm3.jpeg"], date: '17/03/16', user: window.localStorage.getItem('user'), text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsum eum molestiae at autem provident aliquam, impedit libero. Quia, itaque.'}];
  historyItemPage = HistoryItemPage;
  reported = this.navParams.data.report;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log(this.navParams.data.report)
  }

  back() {
    this.navCtrl.pop();
  }

  historyItem(report) {
    this.navCtrl.push(this.historyItemPage, {
      src: report.src,
      text: report.text,
      report: this.reported
    });
  }
}
