import { Component, Input } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

import { FieldPage } from '../field/field';
import { MapPage } from '../map/map';
import { LoginPage } from '../login/login'
import { GuardServiceProvider } from '../../providers/guard-service/guard-service';
import { TokenServiceProvider } from '../../providers/token-service/token-service';
import { GetFieldsProvider } from '../../providers/get-fields/get-fields';
import { UrlServiceProvider } from '../../providers/url-service/url-service';
import { GetUserServiceProvider } from '../../providers/get-user-service/get-user-service';



@Component({
  selector: 'page-inspection',
  templateUrl: 'inspection.html',
})

export class InspectionPage {
  //DECLARATIONSs
  confirm: string = 'pedding';
  username: string;
  searchTerm: string;
  results: any = [];
  fields = [];
  fieldPage = FieldPage;
  mapPage = MapPage;
  loginPage = LoginPage;
  animateClass: any;
  //CONSTRUCTOR
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private guardServiceProvider: GuardServiceProvider,
     private tokenServiceProvider: TokenServiceProvider,
     private app: App,
     private getFields: GetFieldsProvider,
     private getUser: GetUserServiceProvider) {
       this.getFields.getField()
       .subscribe(
         res => {
           for(let i = 0; i < res.length; i++) {
             setTimeout( () => {
               this.fields.push(res[i]);
             }, 300 * i)
           }
         },
         err => {
           console.log(err);
         }
       )
       this.animateClass = { 'fade-in-left-item': true };
       getUser.getUser()
       .subscribe(
         res => {
           this.username = res.user;
           window.localStorage.setItem('user', res.name)
         },
         err => {
           console.log(err);
         }
       )
     }
     //ION CYCLE
     ionViewWillLeave() {
       if(!this.guardServiceProvider.isAuth()) {
         this.app.getRootNav().push(this.loginPage);
       }
     }

     ionViewWillEnter() {
       if(this.navParams.data.confirm) {
         this.confirm = this.navParams.data.confirm;
       }
     }
  //PAGE FUNCTIONS
  details(i: number) {
      console.log(i);
      this.app.getRootNav().push(this.fieldPage, {
        field: i,
        confirm: this.confirm,
      });
  }

  logout() {
    this.tokenServiceProvider.deleteToken();
    this.app.getRootNav().push(this.loginPage);
  }

  searchBar() {
    this.results = this.fields.filter( (el) => {
      return el.id == this.searchTerm;
    })
  }
}
