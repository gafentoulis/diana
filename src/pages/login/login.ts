import { Component} from '@angular/core';
import { NavController, AlertController, MenuController } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TokenServiceProvider } from '../../providers/token-service/token-service';
import { GuardServiceProvider } from '../../providers/guard-service/guard-service';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  username: string;
  password: string;
  tabsPage = TabsPage;

  constructor(private authService: AuthServiceProvider,
  private navCtrl: NavController,
  private alertCtrl: AlertController,
  private guardServiceProvider: GuardServiceProvider,
  private tokenServiceProvider: TokenServiceProvider,
  public menu: MenuController) {}
  //Alert if there error with token
  loginError() {
    let alert = this.alertCtrl.create({
      title: 'Token check',
      message: 'Wrong Authentication'
    });
    alert.present();
  }
  //Alert if you give wrong username or pass
  wrongCredentials() {
    let alert = this.alertCtrl.create({
      title: 'Authentication Error',
      message: 'Wrong Username or Password',
      buttons: [
        {text: 'Try Again'}
      ]
    })
    alert.present();
  }
  //Checking if the Auth is correct and pass the login page
  ionViewWillEnter () {
    if (this.guardServiceProvider.isAuth()) {
      this.navCtrl.push(this.tabsPage);
    }
  }
  //The function for login
  logForm() {
    this.authService.login(this.username, this.password)
      .subscribe(
        res => {
          if(this.guardServiceProvider.isAuth) {
            this.tokenServiceProvider.setToken(res.access_token, res.expires_in + Date.now());
            this.navCtrl.push(this.tabsPage);
          } else {
            this.loginError();
          }
        },
      (err) => this.wrongCredentials()
    )
  }
}
