import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

import { GuardServiceProvider } from '../../providers/guard-service/guard-service';
import { GetFieldsProvider } from '../../providers/get-fields/get-fields';
import { LoginPage } from '../login/login';
import leaflet from 'leaflet';


@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  providers: [GuardServiceProvider, GetFieldsProvider]
})
export class MapPage {
  loginPage = LoginPage;
  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  public fields = new Array();
  Element: any;
  states: any;
  node: any;
  div: any

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private guardServiceProvider: GuardServiceProvider,
     private getFields: GetFieldsProvider,
     private app: App) {
       this.getFields.getField()
       .subscribe(
         res => {
           console.log(res);
           res.map( (el) => {
             this.fields.push(el.water_rights_italy.geom);
           })
         },
         err => {
           console.log(err);
         },
         () => {
           this.loadmap();
         }
       )
  }

 loadmap() {
    var vm = this
    this.map = leaflet.map("map").fitWorld();
    this.map.setView([41, 14], 8);
    leaflet.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ2FmZW50b3VsaXMiLCJhIjoiY2ppcTF1cHE4MHF6YzNxcGVxNmN2NWkwNyJ9.YVhHL3fSXu7S3kKhEmbCpQ', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18,
      minZoom: 3
    }).addTo(this.map);
    this.map.locate({
      maxZoom: 15
    }).on('locationfound', (e) => {
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([e.latitude, e.longitude]);
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup);
    })
    leaflet.polygon(this.fields).addTo(this.map);
    setTimeout(function() {
    vm.map.flyTo([41.3119603283365, 14.3651660508938], 10, {
      headingDegrees: 270,
      animate: true,
      pan: {
        duration:200
      }
    });
  }, 3000);
  }

  ionViewWillLeave() {
    if(!this.guardServiceProvider.isAuth()) {
      this.app.getRootNav().push(this.loginPage);
    }
  }
}
