import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { PhotoServiceProvider } from '../../providers/photo-service/photo-service';

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
  providers: [Camera]
})
export class ModalPage {

  photos: any = [];
  photoSend: any;
  title: string;
  textReport: string;
  options: CameraOptions = {
    quality: 100,
    targetWidth: 900,
    targetHeight: 600,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    saveToPhotoAlbum: false,
    sourceType: 1

  }

  private openGallery (): void {

  let cameraOptions = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    quality: 100,
    targetWidth: 1000,
    targetHeight: 1000,
    encodingType: this.camera.EncodingType.JPEG,
    correctOrientation: true
  }

  this.camera.getPicture(cameraOptions)
    .then( (file_url) => {
      this.photos.push('data:image/jpeg;base64,' + file_url);
    },
    err => console.log(err));
  }



  constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera, private photoService: PhotoServiceProvider) {
  }

  takePhoto() {
    this.camera.getPicture(this.options)
      .then((imageData) => {
        this.photos.push('data:image/jpeg;base64,' + imageData);
        this.photoSend = imageData;
      },(err) => {
        console.log(err);
     });
  }
   report() {
     this.photoService.postPhoto(this.photoSend, this.navParams.data.field.water_rights_italy.id, this.navParams.data.field.inspector_id, this.title, this.textReport)
     this.navCtrl.pop();
   }

   back() {
     this.navCtrl.pop();
   }

}
