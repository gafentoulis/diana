import { Component } from '@angular/core';

import { InspectionPage } from '../inspection/inspection';
import { MapPage } from '../map/map';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = InspectionPage;
  tab2Root = MapPage;


  constructor() {

  }
}
