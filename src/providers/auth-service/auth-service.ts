import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { UrlServiceProvider } from '../url-service/url-service';

@Injectable()
export class AuthServiceProvider {
  url: string;
  constructor(private http: HttpClient, urlService: UrlServiceProvider) {
    this.url = urlService.url
  }

  login(username: string, password: string) {
    return this.http.post<any>(`http://${this.url}/oauth/token`,
    {
      "client_secret":"ztRFgtJwhLRUtqrGULoiVyuJGSWAcRiTlDltmjpt",
      "username": username,
      "password": password,
      "grant_type": "password",
      "client_id" : 2});
  }

}
