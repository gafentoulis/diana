import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlServiceProvider } from '../url-service/url-service';

@Injectable()
export class GetFieldsProvider {
  url: string;
  constructor(public http: HttpClient,
  private urlService: UrlServiceProvider) {
    this.url = urlService.url
  }

  getField() {
    return this.http.get<any>(`http://${this.url}/field/get/user/15`);
  }

}
