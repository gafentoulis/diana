import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlServiceProvider } from '../url-service/url-service';

@Injectable()
export class GetUserServiceProvider {
  url: string;
  constructor(public http: HttpClient, private urlService: UrlServiceProvider) {
    this.url = urlService.url
  }

  getUser() {
    const httpOptions = {
     headers: new HttpHeaders({
       "Content-Type": "application/json",
       Authorization:
         `Bearer ${localStorage.getItem('token')}`
     })
   };
   return this.http.get<any>(`http://${this.url}/api/user`, httpOptions);
  }

}
