import { Injectable } from '@angular/core';

import { TokenServiceProvider } from '../token-service/token-service';


@Injectable()
export class GuardServiceProvider {

  constructor(private tokenServiceProvider: TokenServiceProvider) {}
  
  isAuth() {
    if (this.tokenServiceProvider.getToken()) {
      return true
    } else {
      return false;
    }
  }

}
