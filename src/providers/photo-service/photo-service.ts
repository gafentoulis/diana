import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { UrlServiceProvider } from '../url-service/url-service';

@Injectable()
export class PhotoServiceProvider {
  url: string;
  constructor(public http: HttpClient, urlService: UrlServiceProvider) {
    this.url = urlService.url
  }

  postPhoto (photoUrl: string, field_id: number, inspector_id: number, title: string, textReport: string) {
    this.http.post(`http://${this.url}/api/field/photos/upload`,{
      "photoUrl": photoUrl,
      "field_id":field_id,
      "inspector_id":inspector_id,
      "title": title,
      "textReport": textReport
    })
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  getPhoto (field_id: number) {
    return this.http.get<any>(`http://${this.url}/api/photo/get/field/${field_id}`)
  }
}
