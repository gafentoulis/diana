import { Injectable } from '@angular/core';

@Injectable()
export class TokenServiceProvider {

  getToken() {
    const token = localStorage.getItem('token');
    const expiration = localStorage.getItem('expiration');

    if( !token || !expiration)
      return null
    if(Date.now() > parseInt(expiration)) {
      this.deleteToken();
        return null;
    } else {
      return token;
    }
  }

  deleteToken() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
  }

  setToken(token, expiration) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expiration);
  }
}
